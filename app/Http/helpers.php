<?php

if (! function_exists('base64url_encode')) {
    /**
     * @param mixed $data
     * @return string
     */
    function base64url_encode($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }
}

if (! function_exists('base64url_decode')) {
    /**
     * @param mixed $data
     * @return string
     */
    function base64url_decode($data)
    {
        return base64_decode(strtr($data, '-_', '+/') . str_repeat('=', 3 - (3 + strlen($data)) % 4));
    }
}

if (! function_exists('random_str')) {
    function random_str(int $length, string $keyspace = ''): string
    {
        $keyspace = $keyspace ?: '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces[] = $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }
}

if (! function_exists('create_array_for_phone')) {
    function create_array_for_phone(?string $phoneNumber): ?array
    {
        $value = normalize_string($phoneNumber);

        if (! is_numeric($value)) {
            return null;
        }

        $phones = [];
        if (substr($value, 0, 2) == '08') {
            $phones['international'] = '0' . substr($value, 1, strlen($value));
            $phones['country_id'] = '62' . substr($value, 1, strlen($value));
            $phones['phone'] = substr($value, 1, strlen($value));
            return $phones;
        }

        if (substr($value, 0, 2) == '62') {
            $phones['international'] = '0' . substr($value, 2, strlen($value));
            $phones['country_id'] = '62' . substr($value, 2, strlen($value));
            $phones['phone'] = substr($value, 2, strlen($value));
            return $phones;
        }

        if (substr($value, 0, 3) == '+62') {
            $phones['international'] = '0' . substr($value, 3, strlen($value));
            $phones['country_id'] = '62' . substr($value, 3, strlen($value));
            $phones['phone'] = substr($value, 3, strlen($value));
            return $phones;
        }

        return [
            'international' => "0". $value,
            'country_id' => "62" . $value,
            'phone' => $value,
        ];
    }
}

if (! function_exists('normalize_string')) {
    /**
     * @param  string  $value
     * @return string|null
     */
    function normalize_string($value)
    {
        $value = str_replace('-', '', $value);

        return preg_replace('/[^A-Za-z0-9\-]/', '', $value);
    }
}