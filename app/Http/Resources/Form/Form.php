<?php

namespace App\Http\Resources\Form;

use Illuminate\Support\Arr;

abstract class Form
{
    /** @var array */
    protected $parameters = [];

    /**
     * Get value from parameters using dot notation
     *
     * @param  string $key
     * @param  mixed  $default
     * @return mixed
     */
    protected function get($key, $default = null)
    {
        return Arr::get($this->parameters, $key, $default);
    }
}

