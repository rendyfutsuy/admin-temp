<?php

namespace App\Http\Resources;

use App\Http\Resources\Form\Form;
use Illuminate\Support\Facades\Config;

class ProfileForms extends Form
{
    /** @var array */
    protected $parameters = [];

    /**
     * @param  array $parameters
     * @return void
     */
    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     *
     * @return array
     */
    public function render()
    {
        $meta =  $this->getMeta();

        return [
            'birthday' => [
                'meta' => $meta,
            ],
            'full_name' => [
                'meta' => $meta,
            ],
            'username' => [
                'username' => $this->get('username', auth()->user()->username),
            ],
            'gender' => [
                'meta' => $meta,
            ],
        ];
    }

    protected function getMeta(): array
    {
        $meta = Config::get('profile.meta');
        $results = [];

        foreach ($meta as $data) {
            if ($this->get($data, null)) {
                $results[$data] = $this->get($data);
            }
        }

        return $results;
    }
}
