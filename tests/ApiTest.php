<?php

namespace Tests;

use Illuminate\Support\Facades\Auth;

abstract class ApiTest extends TestCase
{
    protected function generateHeader(int $id): array
    {
        return [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . auth('api')->tokenById($id),
        ];
    }
}
