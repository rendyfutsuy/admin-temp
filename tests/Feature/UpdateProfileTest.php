<?php

namespace Tests\Feature;

use Tests\TestCase;
use Modules\Auth\Models\UserActivity;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateProfileTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function user_can_update_their_profile()
    {
        $this->signIn();

        $response = $this->postJson(route('auth.profile.update'), [
            'username' => 'rendyangga',
            'full_name' => 'Rendy Anggara',
        ]);

        $response->assertOk();

        $this->assertDatabaseHas('users', [
            'id' => auth()->id(),
            'username' => 'rendyangga',
            'meta' => json_encode([
                'full_name' => 'Rendy Anggara',
            ]),
        ]);

        $this->assertDatabaseHas('user_activities', [
            'user_id' => auth()->id(),
            'type' => UserActivity::BIODATA,
            'activity_code' => UserActivity::PROFILE_USERNAME_UPDATED,
        ]);
    }

    /** @test */
    public function when_update_full_name_log_added()
    {
        $this->signIn();

        $response = $this->postJson(route('auth.profile.update'), [
            'username' => 'rendyangga',
            'full_name' => 'Rendy Anggara',
        ]);

        $response->assertOk();

        $this->assertDatabaseHas('user_activities', [
            'user_id' => auth()->id(),
            'type' => UserActivity::BIODATA,
            'activity_code' => UserActivity::PROFILE_FULL_NAME_UPDATED,
        ]);
    }

    /** @test */
    public function when_update_gender_log_added()
    {
        $this->signIn();

        $response = $this->postJson(route('auth.profile.update'), [
            'username' => 'rendyangga',
            'gender' => 1,
        ]);

        $response->assertOk();

        $this->assertDatabaseHas('user_activities', [
            'user_id' => auth()->id(),
            'type' => UserActivity::BIODATA,
            'activity_code' => UserActivity::PROFILE_GENDER_UPDATED,
        ]);
    }

    /** @test */
    public function when_update_birthday_log_added()
    {
        $this->signIn();

        $response = $this->postJson(route('auth.profile.update'), [
            'username' => 'rendyangga',
            'birthday' => 1,
        ]);

        $response->assertOk();

        $this->assertDatabaseHas('user_activities', [
            'user_id' => auth()->id(),
            'type' => UserActivity::BIODATA,
            'activity_code' => UserActivity::PROFILE_BIRTHDAY_UPDATED,
        ]);
    }

    /** @test */
    public function guest_can_not_update_their_profile()
    {
        $this->withExceptionHandling();

        $response = $this->postJson(route('auth.profile.update'), [
            'username' => 'rendyangga',
            'full_name' => 'Rendy Anggara',
        ]);

        $response->assertUnauthorized();
    }
}
