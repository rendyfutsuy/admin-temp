<?php

namespace Tests\Feature;

use Modules\Auth\Models\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Support\Facades\Config;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SkipActivationTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Config::set('auth.skip_activation', true);
    }

    /** @test */
    public function mark_user_as_verified_when_skip_activation_is_true()
    {
        Carbon::setTestNow('2019-08-07 16:11:54');
        
        $response = $this->post(route('register'), [
            'email' => 'aditia@holahalo.com',
            'username' => 'aditia',
            'password' => 'secret',
            'password_confirmation' => 'secret',
        ])->assertRedirect();

        $this->assertDatabaseHas('users', [
            'email' => 'aditia@holahalo.com',
            'username' => 'aditia',
            'email_verified_at' => '2019-08-07 16:11:54',
        ]);
    }

    /** @test */
    public function can_register_user_from_sign_in_guest_user()
    {
        Carbon::setTestNow('2019-08-07 16:11:54');

        $user = factory(User::class)->create([
            'email' => null,
            'username' => 'guest123',
            'level' => User::GUEST,
        ]);

        $this->signIn($user);

        $response = $this->post(route('register'), [
            'user_id' => $user->id,
            'email' => 'aditia@holahalo.com',
            'username' => 'aditia',
            'password' => 'secret',
            'password_confirmation' => 'secret',
        ])->assertRedirect();

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'email' => 'aditia@holahalo.com',
            'username' => 'aditia',
            'email_verified_at' => '2019-08-07 16:11:54',
            'level' => User::REGISTERED,
        ]);
    }
}