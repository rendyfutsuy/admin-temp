<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Modules\Auth\Models\UserActivity;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UploadAvatarTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function user_can_update_their_profile_avatar()
    {
        $this->signIn();

        Storage::fake();

        $response = $this->postJson(route('auth.profile.upload.avatar'), [
            'file' => UploadedFile::fake()->image('my-avatar.png'),
        ]);

        $response->assertOk();

        $file = auth()->user()->avatar;

        Storage::assertExists($file);
    }

    /** @test */
    public function add_log_to_record_avatar_was_uploded()
    {
        $this->signIn();

        Storage::fake();

        $response = $this->postJson(route('auth.profile.upload.avatar'), [
            'file' => UploadedFile::fake()->image('my-avatar.png'),
        ]);

        $response->assertOk();

        $file = auth()->user()->avatar;

        $this->assertDatabaseHas('user_activities', [
            'user_id' => auth()->id(),
            'type' => UserActivity::BIODATA,
            'activity_code' => UserActivity::PROFILE_AVATAR_UPDATED,
        ]);
    }

    /** @test */
    public function guest_can_not_update_their_profile_avatar()
    {
        $this->withExceptionHandling();

        $response = $this->postJson(route('auth.profile.upload.avatar'), [
            'file' => UploadedFile::fake()->image('my-avatar.png'),
        ]);

        $response->assertUnauthorized();
    }

    /** @test */
    public function avatar_is_required()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $response = $this->postJson(route('auth.profile.upload.avatar'), [
            'file' => null,
        ]);

        $response->assertJsonValidationErrors('file');
    }

    /** @test */
    public function avatar_must_be_file()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $response = $this->postJson(route('auth.profile.upload.avatar'), [
            'file' => 'my-image.png',
        ]);

        $response->assertJsonValidationErrors('file');
    }

    /** @test */
    public function avatar_must_be_less_than_5mb_in_size()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $response = $this->postJson(route('auth.profile.upload.avatar'), [
            'file' => UploadedFile::fake()->image('my-avatar.png')->size(6000),
        ]);

        $response->assertJsonValidationErrors('file');
    }

    /** @test */
    public function avatar_must_be_image_format()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $response = $this->postJson(route('auth.profile.upload.avatar'), [
            'file' => UploadedFile::fake()->image('my-avatar.pdf'),
        ]);

        $response->assertJsonValidationErrors('file');
    }
}
