<?php

namespace Modules\Auth\Tests\Feature\Api;

use Modules\Auth\Models\User;
use Tests\TestCase;
use Modules\Auth\Emails\UserActivationEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiUserRegistrationTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        Mail::fake();
    }

    /** @test */
    public function guest_can_register_new_from_api()
    {
        $this->post(route('api.auth.register'), $this->validParams());

        $user = User::first();

        $this->assertGuest();

        $this->assertFalse($user->isActivated());
        $this->assertNotNull($user->getActivationCode());

        $this->assertDatabaseHas('users', [
            'email' => 'fajar@gmail.com',
            'username' => 'fajarr',
        ]);
    }

    /** @test */
    public function assert_profile_is_null_after_from_api()
    {
        $this->post(route('api.auth.register'), $this->validParams());

        $user = User::first();

        $this->assertGuest();

        $this->assertNull($user->fresh()->profile);
    }

    /** @test */
    public function a_confirmation_email_is_sent_upon_registration_from_api()
    {
        $this->post(route('api.auth.register'), $this->validParams());

        Mail::assertQueued(UserActivationEmail::class);
    }

    /** @test */
    public function guests_cannot_register_using_registered_email_from_api()
    {
        $this->withExceptionHandling();

        factory(User::class)->states('activated')->create([
            'email' => 'fajar@gmail.com',
        ]);

        $response = $this->post(route('api.auth.register'), $this->validParams([
            'email' => 'fajar@gmail.com',
        ]));

        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function user_must_input_a_valid_email_upon_registration_from_api()
    {
        $this->withExceptionHandling();

        $response = $this->post(route('api.auth.register'), $this->validParams([
            'email' => 'invalid email',
        ]));

        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function email_is_required_upon_registration_from_api()
    {
        $this->withExceptionHandling();

        $response = $this->post(route('api.auth.register'), $this->validParams([
            'email' => '',
        ]));

        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function name_must_be_equal_or_less_than_128_characters_upon_registration_from_api()
    {
        $this->withExceptionHandling();

        $response = $this->post(route('api.auth.register'), $this->validParams([
            'username' => random_str(129),
        ]));

        $response->assertSessionHasErrors('username');
    }

    /** @test */
    public function name_is_required_upon_registration_from_api()
    {
        $this->withExceptionHandling();

        $response = $this->post(route('api.auth.register'), $this->validParams([
            'username' => '',
        ]));

        $response->assertSessionHasErrors('username');
    }

    /** @test */
    public function password_must_be_6_or_more_characters_upon_registration_from_api()
    {
        $this->withExceptionHandling();

        $response = $this->post(route('api.auth.register'), $this->validParams([
            'password' => '1234',
        ]));

        $response->assertSessionHasErrors('password');
    }

    /** @test */
    public function password_is_required_upon_registration_from_api()
    {
        $this->withExceptionHandling();

        $response = $this->post(route('api.auth.register'), $this->validParams([
            'password' => '',
        ]));

        $response->assertSessionHasErrors('password');
    }

    /** Set up register data. */
    protected function validParams(array $overrides = []): array
    {
        return array_merge([
            'email' => 'fajar@gmail.com',
            'username' => 'fajarr',
            'password' => 'rahasia',
        ], $overrides);
    }
}
