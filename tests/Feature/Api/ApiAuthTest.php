<?php

namespace Modules\Auth\Tests\Feature\Api;

use Modules\Auth\Models\User;
use Tests\ApiTest;
use Modules\Auth\Models\UserActivity;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiAuthTest extends ApiTest
{
    use RefreshDatabase;

    /** @test */
    public function user_can_login_from_api()
    {
        $this->validParams();

        $body = [
            'login'    => 'example@gmail.com',
            'password' => 'secret',
        ];

        $this->post(route('api.auth.login'), $body)
            ->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'token_type',
                'expires_in',
            ]);
    }

    /** @test */
    public function login_history_created_when_login_from_api()
    {
        $this->validParams();

        $body = [
            'login'    => 'example@gmail.com',
            'password' => 'secret',
        ];

        $this->post(route('api.auth.login'), $body)
            ->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'token_type',
                'expires_in',
            ]);

        $this->assertDatabaseHas('user_activities', [
            'user_id' => auth('api')->user()->id,
            'type' => UserActivity::ACTIVITY,
            'activity_code' => UserActivity::LOGIN,
        ]);
    }

    /** @test */
    public function user_can_not_login_using_unregistered_email_from_api()
    {
        $this->withExceptionHandling();

        $body = [
            'login'    => 'unregistered@gmail.com',
            'password' => 'secret',
        ];

        $this->post(route('api.auth.login'), $body)
            ->assertStatus(401);
    }

    /** @test */
    public function user_can_not_login_using_un_activated_account_from_api()
    {
        factory(User::class)->states('needs_activation')->create([
            'email' => 'example@gmail.com',
            'password' => bcrypt('secret'),
        ]);

        $body = [
            'login'    => 'example@gmail.com',
            'password' => 'secret',
        ];

        $this->post(route('api.auth.login'), $body)
            ->assertStatus(422)
            ->assertJson([
                'status' => 'Fail',
            ]);
    }

    /** @test */
    public function user_can_refresh_access_token_from_api()
    {
        $this->signIn();

        $headers = $this->generateHeader(auth()->id());

        $this->post(route('api.auth.refresh'), [], $headers)
            ->assertJsonStructure([
                'access_token',
                'token_type',
                'expires_in',
            ]);
    }

    /** @test */
    public function user_can_logout_from_api()
    {
        $this->signIn()
            ->withExceptionHandling();

        $headers = $this->generateHeader(auth()->id());

        $this->postJson(route('api.auth.logout'), [], $headers)
            ->assertJsonStructure([
                'message',
            ]);
    }

    /** @test */
    public function logout_history_created_when_user_logged_out_from_api()
    {
        $this->signIn();

        $userId = collect(auth()->user()->id);

        $headers = $this->generateHeader(auth()->id());

        $this->postJson(route('api.auth.logout'), [], $headers)
            ->assertJsonStructure([
                'message',
            ]);

        $this->assertDatabaseHas('user_activities', [
            'user_id' => $userId[0],
            'type' => UserActivity::ACTIVITY,
            'activity_code' => UserActivity::LOGOUT,
        ]);
    }

    /** @test */
    public function user_should_insert_correct_password_from_api()
    {
        $this->validParams();

        $body = [
            'login'    => 'example@gmail.com',
            'password' => 'wrong password',
        ];

        $this->post(route('api.auth.login'), $body)
            ->assertStatus(401)
            ->assertJsonStructure([
                'message',
            ]);
    }

    /**
     * Set up user data.
     *
     * @return \Modules\Auth\Models\User
     */
    protected function validParams(array $override = [])
    {
        return factory(User::class)->states('activated')->create([
            'email' => 'example@gmail.com',
            'password' => bcrypt('secret'),
        ], $override);
    }
}
