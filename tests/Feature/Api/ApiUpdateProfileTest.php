<?php

namespace Tests\Feature\Api;

use Tests\ApiTest;
use Modules\Auth\Models\UserActivity;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiUpdateProfileTest extends ApiTest
{
    use RefreshDatabase;
    
    /** @test */
    public function user_can_update_their_profile_from_api()
    {
        $user = factory(\Modules\Auth\Models\User::class)->create();

        $headers = $this->generateHeader($user->id);

        $response = $this->postJson(route('api.auth.profile.update'), [
            'username' => 'rendyangga',
            'full_name' => 'Rendy Anggara',
        ], $headers);

        $response->assertOk();

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'username' => 'rendyangga',
            'meta' => json_encode([
                'full_name' => 'Rendy Anggara',
            ]),
        ]);
    }

    /** @test */
    public function when_update_full_name_log_added()
    {
        $this->signIn();

        $user = factory(\Modules\Auth\Models\User::class)->create();

        $headers = $this->generateHeader($user->id);

        $response = $this->postJson(route('api.auth.profile.update'), [
            'username' => 'rendyangga',
            'full_name' => 'Rendy Anggara',
        ], $headers);

        $response->assertOk();

        $this->assertDatabaseHas('user_activities', [
            'user_id' => auth()->id(),
            'type' => UserActivity::BIODATA,
            'activity_code' => UserActivity::PROFILE_FULL_NAME_UPDATED,
        ]);
    }

    /** @test */
    public function when_update_gender_log_added()
    {
        $this->signIn();

        $user = factory(\Modules\Auth\Models\User::class)->create();

        $headers = $this->generateHeader($user->id);

        $response = $this->postJson(route('api.auth.profile.update'), [
            'username' => 'rendyangga',
            'gender' => 1,
        ], $headers);

        $response->assertOk();

        $this->assertDatabaseHas('user_activities', [
            'user_id' => auth()->id(),
            'type' => UserActivity::BIODATA,
            'activity_code' => UserActivity::PROFILE_GENDER_UPDATED,
        ]);
    }

    /** @test */
    public function when_update_birthday_log_added()
    {
        $this->signIn();

        $user = factory(\Modules\Auth\Models\User::class)->create();

        $headers = $this->generateHeader($user->id);

        $response = $this->postJson(route('api.auth.profile.update'), [
            'username' => 'rendyangga',
            'birthday' => 1,
        ], $headers);

        $response->assertOk();

        $this->assertDatabaseHas('user_activities', [
            'user_id' => auth()->id(),
            'type' => UserActivity::BIODATA,
            'activity_code' => UserActivity::PROFILE_BIRTHDAY_UPDATED,
        ]);
    }

    /** @test */
    public function guest_can_not_update_their_profile_from_api()
    {
        $this->withExceptionHandling();

        $response = $this->postJson(route('api.auth.profile.update'), [
            'username' => 'rendyangga',
            'full_name' => 'Rendy Anggara',
        ]);

        $response->assertUnauthorized();
    }
}
