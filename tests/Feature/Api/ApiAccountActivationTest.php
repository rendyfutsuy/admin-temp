<?php

namespace Modules\Auth\Tests\Feature\Api;

use Modules\Auth\Models\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiAccountActivationTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Mail::fake();
    }

    /** @test */
    public function user_can_activate_their_account_using_activation_code_from_api()
    {
        $user = $this->validRegistration();

        $body = [
            'email' => base64url_encode($user->email),
            'activation_code' => 123456,
        ];

        $this->post(route('api.auth.activate.account', $body));

        tap($user->fresh(), function ($user) {
            $this->assertTrue($user->isActivated());
            $this->assertNull($user->getActivationCode());
        });
    }

    /** @test */
    public function assert_profile_is_null_after_activating_account_from_api()
    {
        $user = $this->validRegistration();

        $body = [
            'email' => base64url_encode($user->email),
            'activation_code' => 123456,
        ];

        $this->post(route('api.auth.activate.account', $body));

        $this->assertNull($user->fresh()->profile);
    }

    /** @test */
    public function activation_code_is_required_upon_activates_account_on_api()
    {
        $this->withExceptionHandling();

        $user = $this->validRegistration();

        $body = [
            'email' => base64url_encode($user->email),
            'activation_code' => null,
        ];

        $this->postJson(route('api.auth.activate.account'), $body)
            ->assertJsonValidationErrors('activation_code');
    }

    /** @test */
    public function email_is_required_upon_activating_account_on_api()
    {
        $this->withExceptionHandling();

        $this->validRegistration();

        $body = [
            'email' => null,
            'activation_code' => 123456,
        ];

        $this->postJson(route('api.auth.activate.account'), $body)
            ->assertJsonValidationErrors('email');
    }

    /** @test */
    public function user_should_insert_a_correct_email_upon_registration_on_api()
    {
        $this->withExceptionHandling();

        $this->validRegistration();

        $body = [
            'email' => base64url_encode('wrong@email.com'),
            'activation_code' => '123456',
        ];

        $this->postJson(route('api.auth.activate.account'), $body)
            ->assertUnauthorized();
    }

    /** @test */
    public function user_has_access_token_after_activating_account_from_api()
    {
        $user = $this->validRegistration();

        $body = [
            'email' => base64url_encode($user->email),
            'activation_code' => 123456,
        ];

        $response = $this->post(route('api.auth.activate.account', $body));

        $response->assertJsonStructure([
            'message',
            'access_token',
            'token_type',
            'expires_in',
        ]);
    }

    /** @test */
    public function last_online_is_recorded_after_successful_activation_from_api()
    {
        $user = $this->validRegistration();

        $body = [
            'email' => base64url_encode($user->email),
            'activation_code' => 123456,
        ];

        $this->post(route('api.auth.activate.account', $body));

        $this->assertNotNull($user->fresh()->last_online);
    }

    /** @test */
    public function user_should_insert_correct_activation_code_from_api()
    {
        $this->withExceptionHandling();

        $user = $this->validRegistration();

        $body = [
            'email' => base64url_encode($user->email),
            'activation_code' => 111111,
        ];

        $response = $this->post(route('api.auth.activate.account', $body));

        $response->json([
            'message' => 'Kode yang Anda masukkan salah',
        ]);

        tap($user->fresh(), function ($user) {
            $this->assertFalse($user->isActivated());
            $this->assertNotNull($user->getActivationCode());
        });
    }

    /** @test */
    public function user_can_not_activate_account_that_already_activated_from_api()
    {
        $this->withExceptionHandling();

        $user = factory(User::class)->states('activated')->create();

        $body = [
            'email' => base64url_encode($user->email),
            'activation_code' => 11111,
        ];

        $this->post(route('api.auth.activate.account', $body))
            ->assertStatus(422);
    }

    /** @test */
    public function user_can_not_resend_activation_code_using_wrong_email_from_api()
    {
        $body = [
            'email' => 'wrong@gmail.com',
        ];

        $this->post(route('api.auth.resend.activation.code', $body))
            ->assertUnauthorized();
    }

    /** @test */
    public function email_is_required_to_resend_activation_code_from_api()
    {
        $this->withExceptionHandling();

        $body = [
            'email' => '',
        ];

        $this->post(route('api.auth.resend.activation.code', $body))
            ->assertSessionHasErrors('email');
    }

    /**
     * Create valid user registration.
     *
     * @return \Modules\Auth\Models\User
     */
    protected function validRegistration(array $override = [])
    {
        return factory(User::class)->states('needs_activation')->create([
            'activation_code' => 123456,
        ], $override);
    }
}
