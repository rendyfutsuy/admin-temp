<?php

namespace Modules\Auth\Tests\Feature\Api;

use Modules\Auth\Models\User;
use Tests\TestCase;
use Modules\Auth\Emails\UserResetPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiResetPasswordTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        Mail::fake();
    }

    /** @test */
    public function registered_users_can_reset_their_password_from_api()
    {
        $user = factory(User::class)
            ->states('activated')
            ->create(['activation_code' => null]);

        $this->post(route('api.auth.send.reset.code'), [
            'email' => $user->email,
        ])
            ->assertStatus(200);

        Mail::assertQueued(UserResetPassword::class);
        $this->assertNotNull($user->fresh()->activation_code);

        $this->post(route('api.auth.validate.reset.code'), [
            'email' => base64url_encode($user->email),
            'activation_code' => $user->fresh()->getActivationCode(),
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.reset.password'), [
            'email' => base64url_encode($user->email),
            'activation_code' => $user->fresh()->getActivationCode(),
            'password' => 'newPassword',
            'password_confirmation' => 'newPassword',
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.login'), [
            'login' => $user->email,
            'password' => 'newPassword',
        ])
            ->assertStatus(200);

        $this->assertNull($user->fresh()->activation_code);
        $this->assertTrue(Hash::check('newPassword', $user->fresh()->password));
    }

    /** @test */
    public function registered_users_can_resend_reset_password_verification_email_from_api()
    {
        $user = factory(User::class)
            ->states('activated')
            ->create(['activation_code' => null]);

        $this->post(route('api.auth.send.reset.code'), [
            'email' => $user->email,
        ]);

        $this->post(route('api.auth.resend.reset.code'), [
            'email' => $user->email,
        ]);

        Mail::assertQueued(UserResetPassword::class);
    }

    /** @test */
    public function email_is_required_to_resend_reset_password_verification_email_from_api()
    {
        $this->withExceptionHandling();

        $user = factory(User::class)
            ->states('activated')
            ->create(['activation_code' => null]);

        $this->post(route('api.auth.resend.reset.code'), [
            'email' => $user->email,
        ]);

        $this->post(route('api.auth.resend.reset.code'), [
            'email' => null,
        ])->assertSessionHasErrors('email');
    }

    /** @test */
    public function user_should_request_to_reset_his_password_before_resend_verification_code_from_api()
    {
        $user = factory(User::class)
            ->states('activated')
            ->create(['activation_code' => null]);

        $this->post(route('api.auth.resend.reset.code'), [
            'email' => $user->email,
        ])
        ->assertStatus(422)
        ->assertJson([
            'status' => 'Fail',
        ]);
    }

    /** @test */
    public function unregistered_users_cannot_reset_password_from_api()
    {
        $this->assertGuest()
            ->withExceptionHandling();

        $this->post(route('api.auth.send.reset.code'), [
            'email' => 'rendy@gundam.com',
        ])
        ->assertSessionHasErrors('email');
    }

    /** @test */
    public function unverified_users_cannot_reset_their_password_from_api()
    {
        $user = factory(User::class)
            ->states('needs_activation')
            ->create(['activation_code' => null]);
        $this->withExceptionHandling();


        $this->post(route('api.auth.send.reset.code'), [
            'email' => $user->email,
        ])
            ->assertStatus(422)
            ->assertJson([
                'status' => 'Fail',
            ]);
    }

    /** @test */
    public function email_is_required_to_send_reset_password_verification_email_from_api()
    {
        $this->assertGuest()
            ->withExceptionHandling();

        $this->from('login');

        $this->post(route('api.auth.send.reset.code'), [
            'email' => null,
        ])
            ->assertSessionHasErrors('email');
    }

    /** @test */
    public function email_and_activation_code_is_required_to_verify_reset_password_from_api()
    {
        $this->assertGuest()
            ->withExceptionHandling();

        $this->from('login');

        $this->post(route('api.auth.validate.reset.code'), [
            'email' => null,
            'activation_code' => null,
        ])
            ->assertSessionHasErrors('email', 'activation_code');
    }

    /** @test */
    public function new_password_is_required_to_reset_the_old_password_from_api()
    {
        $this->assertGuest()
            ->withExceptionHandling();

         $user = factory(User::class)
            ->states('activated')
            ->create(['activation_code' => null]);

        $this->post(route('api.auth.send.reset.code'), [
            'email' => $user->email,
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.validate.reset.code'), [
            'email' => base64url_encode($user->email),
            'activation_code' => $user->fresh()->getActivationCode(),
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.reset.password'), [
            'email' => base64url_encode($user->email),
            'activation_code' => $user->fresh()->getActivationCode(),
            'password' => null,
        ])
            ->assertSessionHasErrors('password');
    }

    /** @test */
    public function password_confirmation_is_required_to_reset_the_password_from_api()
    {
        $this->assertGuest()
            ->withExceptionHandling();

         $user = factory(User::class)
            ->states('activated')
            ->create(['activation_code' => null]);

        $this->post(route('api.auth.send.reset.code'), [
            'email' => $user->email,
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.validate.reset.code'), [
            'email' => base64url_encode($user->email),
            'activation_code' => $user->fresh()->getActivationCode(),
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.reset.password'), [
            'email' => base64url_encode($user->email),
            'activation_code' => $user->fresh()->getActivationCode(),
            'password' => 'newPassword',
            'password_confirmation' => null,
        ])
            ->assertSessionHasErrors('password');
    }

    /** @test */
    public function new_password_must_be_more_than_6_character_upon_reset_the_password_from_api()
    {
        $this->withExceptionHandling();

        $this->assertGuest()
        ->withExceptionHandling();

        $user = factory(User::class)
            ->states('activated')
            ->create(['activation_code' => null]);

        $this->post(route('api.auth.send.reset.code'), [
            'email' => $user->email,
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.validate.reset.code'), [
            'email' => base64url_encode($user->email),
            'activation_code' => $user->fresh()->getActivationCode(),
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.reset.password'), [
            'email' => base64url_encode($user->email),
            'activation_code' => $user->fresh()->getActivationCode(),
            'password' => 'newPa',
            'password_confirmation' => 'newPa',
        ])
            ->assertSessionHasErrors('password');
    }

    /** @test */
    public function password_and_it_confirmation_must_be_same_to_reset_the_password_from_api()
    {
        $this->withExceptionHandling();

        $this->assertGuest()
        ->withExceptionHandling();

        $user = factory(User::class)
            ->states('activated')
            ->create(['activation_code' => null]);

        $this->post(route('api.auth.send.reset.code'), [
            'email' => $user->email,
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.validate.reset.code'), [
            'email' => base64url_encode($user->email),
            'activation_code' => $user->fresh()->getActivationCode(),
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.reset.password'), [
            'email' => base64url_encode($user->email),
            'activation_code' => $user->fresh()->getActivationCode(),
            'password' => 'newPass',
            'password_confirmation' => 'newPassword',
        ])
            ->assertSessionHasErrors('password');
    }

    /** @test */
    public function email_is_required_to_reset_the_password_from_api()
    {
        $this->withExceptionHandling();

        $this->assertGuest()
        ->withExceptionHandling();

        $user = factory(User::class)
            ->states('activated')
            ->create(['activation_code' => null]);

        $this->post(route('api.auth.send.reset.code'), [
            'email' => $user->email,
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.validate.reset.code'), [
            'email' => base64url_encode($user->email),
            'activation_code' => $user->fresh()->getActivationCode(),
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.reset.password'), [
            'email' => null,
            'activation_code' => $user->fresh()->getActivationCode(),
            'password' => 'newPassword',
            'password_confirmation' => 'newPassword',
        ])
            ->assertSessionHasErrors('email');
    }

    /** @test */
    public function email_must_be_valid_to_reset_the_from_api()
    {
        $this->withExceptionHandling();

        $this->assertGuest()
            ->withExceptionHandling();

        $user = factory(User::class)
            ->states('activated')
            ->create(['activation_code' => null]);

        $this->post(route('api.auth.send.reset.code'), [
            'email' => $user->email,
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.validate.reset.code'), [
            'email' => base64url_encode($user->email),
            'activation_code' => $user->fresh()->getActivationCode(),
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.reset.password'), [
            'email' => 'wrong@gmail.com',
            'activation_code' => $user->fresh()->getActivationCode(),
            'password' => 'newPassword',
            'password_confirmation' => 'newPassword',
        ])
            ->assertStatus(422)
            ->assertJson([
                'status' => 'Fail',
            ]);
    }

    /** @test */
    public function activation_code_must_be_valid_to_reset_the_password_from_api()
    {
        $this->withExceptionHandling();

        $this->assertGuest()
            ->withExceptionHandling();

        $user = factory(User::class)
            ->states('activated')
            ->create(['activation_code' => null]);

        $this->post(route('api.auth.send.reset.code'), [
            'email' => $user->email,
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.validate.reset.code'), [
            'email' => base64url_encode($user->email),
            'activation_code' => $user->fresh()->getActivationCode(),
        ])
            ->assertStatus(200);

        $this->post(route('api.auth.reset.password'), [
            'email' => base64url_encode($user->email),
            'activation_code' => 123456,
            'password' => 'newPassword',
            'password_confirmation' => 'newPassword',
        ])
            ->assertStatus(422)
            ->assertJson([
                'status' => 'Fail',
            ]);
    }
}
