<?php

namespace Modules\Auth\Tests\Feature\Api;

use Tests\TestCase;
use Modules\Auth\Models\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Carbon\Carbon;

class ApiSignInAsGuestTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        Event::fake();

        Carbon::setTestNow('2019-08-07 16:11:54');
    }

    /** @test */
    public function guest_can_logged_in_as_guest_user()
    {
        $this->postJson(route('api.auth.login.guest'))
            ->assertOk();

        $user = User::first();

        $this->assertEquals($user, auth('api')->user());

        $this->assertDatabaseHas('users', [
            'username' => '20190807161154',
            'level' => User::GUEST,
            'email_verified_at' => '2019-08-07 16:11:54',
        ]);
    }

    /** @test */
    public function log_as_guest_fire_login_event()
    {
        $this->postJson(route('api.auth.login.guest'))
            ->assertOk();

        $user = User::first();

        Event::assertDispatched(Login::class);
    }

    /** @test */
    public function assert_response_json()
    {
        $this->postJson(route('api.auth.login.guest'))
            ->assertOk()
            ->assertJson([
                'user_id' => auth('api')->id(),
                'name' => auth('api')->user()->username,
                'access_token' => auth('api')->getToken(),
                'token_type' => 'bearer',
                'expires_in' => auth('api')->factory()->getTTL()
            ]);
    }
}
