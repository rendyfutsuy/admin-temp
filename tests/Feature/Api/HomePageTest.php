<?php

namespace Modules\Auth\Tests\Feature\Api;

use Modules\Auth\Models\User;
use Tests\ApiTest;
use Modules\Auth\Models\UserActivity;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomePageTest extends ApiTest
{
    use RefreshDatabase;

    /** @test */
    public function authenticated_user_can_access_home()
    {
        $this->signIn();

        $header = $this->generateHeader(auth()->id());

        $this->getJson(route('api.auth.home'), $header)
            ->assertOk();
    }

    /** @test */
    public function unauthenticated_user_can_not_access_home()
    {
        $this->withExceptionHandling();

        $this->getJson(route('api.auth.home'))
            ->assertUnauthorized();
    }

    /** @test */
    public function user_with_invalid_token_can_not_access_home()
    {
        $header = [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hZG1pbi10ZW1wLnRlc3RcL2FwaVwvdjFcL3B1YmxpY1wvYXV0aFwvbG9naW4iLCJpYXQiOjE1OTY3NzE0MDksImV4cCI6MTU5Njc3NTAwOSwibmJmIjoxNTk2NzcxNDA5LCJqdGkiOiJFTWk1UEUzNk1qNmJ2YUkwIiwic3ViIjoyLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.sYhGUCCPbHTmGE34kzcH1PoyhUwlXJ75i6tTllR9oRM',
        ];

        $this->withExceptionHandling();
        
        $this->getJson(route('api.auth.home'))
            ->assertUnauthorized();
    }
}
