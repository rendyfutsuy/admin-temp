<?php

namespace Tests\Feature\Api;

use Tests\ApiTest;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiUploadAvatarTest extends ApiTest
{
    use RefreshDatabase;
    
    /** @test */
    public function user_can_update_their_profile_avatar()
    {
        $user = factory(\Modules\Auth\Models\User::class)->create();

        $headers = $this->generateHeader($user->id);

        Storage::fake();

        $response = $this->postJson(route('api.auth.profile.upload.avatar'), [
            'file' => UploadedFile::fake()->image('my-avatar.png'),
        ], $headers);

        $response->assertOk();

        $file = auth()->user()->avatar;

        Storage::assertExists($file);
    }

    /** @test */
    public function guest_can_not_update_their_profile_avatar()
    {
        $this->withExceptionHandling();

        $response = $this->postJson(route('api.auth.profile.upload.avatar'), [
            'file' => UploadedFile::fake()->image('my-avatar.png'),
        ]);

        $response->assertUnauthorized();
    }

    /** @test */
    public function unauthorized_user_can_not_update_their_profile_avatar()
    {
        $this->withExceptionHandling();

        $headers = [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer 1234567890',
        ];

        $response = $this->postJson(route('api.auth.profile.upload.avatar'), [
            'file' => UploadedFile::fake()->image('my-avatar.png'),
        ], $headers);

        $response->assertUnauthorized();
    }

    /** @test */
    public function avatar_is_required()
    {
        $this->withExceptionHandling();

        $user = factory(\Modules\Auth\Models\User::class)->create();

        $headers = $this->generateHeader($user->id);

        $response = $this->postJson(route('api.auth.profile.upload.avatar'), [
            'file' => null,
        ], $headers);

        $response->assertJsonValidationErrors('file');
    }

    /** @test */
    public function avatar_must_be_file()
    {
        $this->withExceptionHandling();

        $user = factory(\Modules\Auth\Models\User::class)->create();

        $headers = $this->generateHeader($user->id);

        $response = $this->postJson(route('api.auth.profile.upload.avatar'), [
            'file' => 'my-image.png',
        ], $headers);

        $response->assertJsonValidationErrors('file');
    }

    /** @test */
    public function avatar_must_be_less_than_5mb_in_size()
    {
        $this->withExceptionHandling();

        $user = factory(\Modules\Auth\Models\User::class)->create();

        $headers = $this->generateHeader($user->id);

        $response = $this->postJson(route('api.auth.profile.upload.avatar'), [
            'file' => UploadedFile::fake()->image('my-avatar.png')->size(6000),
        ], $headers);

        $response->assertJsonValidationErrors('file');
    }

    /** @test */
    public function avatar_must_be_image_format()
    {
        $this->withExceptionHandling();

        $user = factory(\Modules\Auth\Models\User::class)->create();

        $headers = $this->generateHeader($user->id);

        $response = $this->postJson(route('api.auth.profile.upload.avatar'), [
            'file' => UploadedFile::fake()->image('my-avatar.pdf'),
        ], $headers);

        $response->assertJsonValidationErrors('file');
    }
}
