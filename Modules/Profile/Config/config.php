<?php

return [
    'name' => 'Profile',
    'meta' => [
        'birthday',
        'full_name',
        'gender',
    ]
];
