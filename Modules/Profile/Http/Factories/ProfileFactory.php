<?php

namespace Modules\Profile\Http\Factories;

use Modules\Auth\Models\User;
use Illuminate\Http\Request;
use Modules\Profile\Http\Builders\Birthday;
use Modules\Profile\Http\Builders\FullName;
use Modules\Profile\Http\Builders\UserName;
use App\Http\Resources\ProfileForms;
use Modules\Profile\ServiceManagers\UserManager;

class ProfileFactory
{
    /** @var User */
    protected $user;

    /** @var Request */
    protected $request;

    /** @var UserManager */
    protected $manager;

    /**
     * @return void
     */
    public function __construct(UserManager $manager, Request $request)
    {
        $this->request = $request;
        $this->manager = $manager;
    }

    protected function builders(?User $user): array
    {
        return [
            new Birthday($user),
            new FullName($user),
            new UserName($user),
        ];
    }

    protected function getParameters(): array
    {
        $forms = new ProfileForms($this->request->all());
        return $forms->render();
    }

    /**
     * @return void
     */
    public function submit(?User $user)
    {
        $this->manager->with($this->builders($user))
            ->commit($this->getParameters());
    }
}
