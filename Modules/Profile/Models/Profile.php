<?php

namespace Modules\Profile\Models;

use Modules\Auth\Models\User;
use Illuminate\Support\Arr;

class Profile extends User
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string|null
     */
    public function getFullNameAttribute()
    {
        return Arr::get($this->meta, 'full_name', null);
    }

    /**
     * @return string|null
     */
    public function getBirthdayAttribute()
    {
        return Arr::get($this->meta, 'birthday', null);
    }

    /**
     * @return string|null
     */
    public function getGenderAttribute()
    {
        return Arr::get($this->meta, 'gender', null);
    }
}
