<?php

use Modules\Profile\Http\Controllers\Api\UploadAvatar;
use Modules\Profile\Http\Controllers\Api\UpdateProfile;

/**
 * Api route di sini, gunakan class name seperti contoh.
 */

// Route::get('url', [ContohController::class, ['index']); contoh dengan nama metho]d
// Route::get('url', ContohInvocable::class); contoh invoke controller

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'private/auth', 'middleware' => ['auth:api', 'has.jwt.token']], function () {
        Route::post('auth/update', UpdateProfile::class)
            ->name('api.auth.profile.update');

        Route::post('auth/upload/avatar', UploadAvatar::class)
            ->name('api.auth.profile.upload.avatar');
    });
});
