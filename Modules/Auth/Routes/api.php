<?php

use Modules\Auth\Http\Controllers\Api\SignInAsGuest;
use Modules\Auth\Http\Controllers\Api\AuthController;
use Modules\Auth\Http\Controllers\Api\HomeController;
use Modules\Auth\Http\Controllers\Api\LoginController;
use Modules\Auth\Http\Controllers\Api\RegisterController;
use Modules\Auth\Http\Controllers\Api\ActivationController;
use Modules\Auth\Http\Controllers\Api\ResetPasswordController;

/**
 * Api route di sini, gunakan class name seperti contoh.
 */

// Route::get('url', [ContohController::class, ['index']); contoh dengan nama metho]d
// Route::get('url', ContohInvocable::class); contoh invoke controller

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'public/auth'], function () {
        Route::post('login', LoginController::class)
            ->name('api.auth.login');
        Route::post('register', RegisterController::class)
            ->name('api.auth.register');
        Route::post('refresh', [AuthController::class, 'refresh'])
            ->name('api.auth.refresh');
        Route::post('activate-account', [ActivationController::class, 'attemptActivation'])
            ->name('api.auth.activate.account');
        Route::post('resend-activation-code', [ActivationController::class, 'resend'])
            ->name('api.auth.resend.activation.code');
        Route::post('send-reset-code', [ResetPasswordController::class, 'sendVerificationEmail'])
            ->name('api.auth.send.reset.code');
        Route::post('resend-reset-code', [ResetPasswordController::class, 'resendVerificationEmail'])
            ->name('api.auth.resend.reset.code');
        Route::post('validate-reset-code', [ResetPasswordController::class, 'validateResetCode'])
            ->name('api.auth.validate.reset.code');
        Route::post('reset-password', [ResetPasswordController::class, 'reset'])
            ->name('api.auth.reset.password');
        Route::post('guest/register', SignInAsGuest::class)
            ->name('api.auth.login.guest');
    });
    Route::group(['prefix' => 'private/auth', 'middleware' => ['auth:api']], function () {
        Route::post('logout', [AuthController::class, 'logout'])
            ->name('api.auth.logout');

        Route::get('home', [HomeController::class, 'index'])
            ->middleware('has.jwt.token')
            ->name('api.auth.home');
    });
});
