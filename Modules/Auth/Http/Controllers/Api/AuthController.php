<?php

namespace Modules\Auth\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Logout;
use App\Http\Controllers\Controller;
use Modules\Auth\Http\Controllers\Auth\CustomAuth\AuthResponses;

class AuthController extends Controller
{
    use AuthResponses;

    const FAIL = 'Fail';
    const SUCCESS = 'Success';

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $userId = auth()->user()->id;

        event(new Logout('api', auth('api')->user()));

        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

     /**
     * Get the token array structure.
     *
     * @param  boolean $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'user_id' => auth('api')->id() ?? null,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL()
        ]);
    }
}
