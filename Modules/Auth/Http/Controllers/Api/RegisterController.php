<?php

namespace Modules\Auth\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Auth\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Events\Registered;
use Modules\Auth\Emails\UserActivationEmail;
use Modules\Auth\Http\Controllers\Auth\CustomAuth\CustomRegistration;

class RegisterController extends CustomRegistration
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['guest']);
    }

    /**
     * Register new user from mobile app.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $this->validator($request->all())->validate();
        
        $user = $this->create($this->registerApplicant($request->all()));

        event(new Registered($user));

        Mail::to($user->email)->send(new UserActivationEmail($user));

        return $this->responseMessage(self::SUCCESS, __('auth.registration.success'), 200);
    }

    protected function myRules(): array
    {
        return [
            'username' => [
                'required',
                'string',
                'max:128',
                Rule::unique('users')->where(function ($query) {
                    return $query->where('level', User::REGISTERED);
                }),
            ],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ];
    }
}
