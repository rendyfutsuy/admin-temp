<?php

namespace Modules\Auth\Http\Controllers\Api;

use Modules\Auth\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Modules\Auth\Emails\UserActivationEmail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Modules\Auth\Http\Controllers\Auth\CustomAuth\AuthResponses;

class ActivationController extends Controller
{
    use AuthResponses;
    
    const FAIL = 'Fail';
    const SUCCESS = 'Success';

    /**
     * Validate activating request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function attemptActivation(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'activation_code' => 'required',
        ]);

        $user = User::whereEmail(base64url_decode($request->email))->first();

        if (! $user) {
            return $this->responseMessage(self::FAIL, 'Unauthenticated', 401);
        }

        if ($user->isActivated()) {
            return $this->responseMessage(self::FAIL, __('auth.activation.email_has_activated'), 422);
        }

        if ($user->activation_code != $request->get('activation_code')) {
            return $this->responseMessage(self::FAIL, __('auth.activation.activation_code_wrong'), 422);
        }

        $user->markEmailAsVerified();
        $user->recordLastOnline();
        $token = JWTAuth::fromUser($user);

        return $this->responseWithToken(self::SUCCESS, __('auth.activation.activation_successed'), $token, 200);
    }

    /**
     * Resend activation code.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resend(Request $request)
    {
        $this->validate($request, ['email' => 'required']);
        $user = User::whereEmail(base64url_decode($request->email))->first();

        if (! $user) {
            return $this->responseMessage(self::FAIL, 'Unauthenticated', 401);
        }

        if ($user->isActivated()) {
            return $this->responseMessage(self::FAIL, __('auth.activation.email_has_activated'), 422);
        }

        Mail::to($user->email)->send(new UserActivationEmail($user));

        return $this->responseMessage(self::SUCCESS, __('auth.activation.send_activation_code_to_email_successed'), 200);
    }
}
