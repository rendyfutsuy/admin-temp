<?php

namespace Modules\Auth\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Auth\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Config;
use Modules\Auth\Emails\UserResetPassword;
use Modules\Auth\Http\Requests\EmailIsExists;
use Modules\Auth\Http\Controllers\Auth\CustomAuth\AuthResponses;

class ResetPasswordController extends Controller
{
    use AuthResponses;
    
    const FAIL = 'Fail';
    const SUCCESS = 'Success';

    /**
     * Send reset password verification to user email.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendVerificationEmail(EmailIsExists $request)
    {
        $user = User::whereEmail($request->email)->first();

        if (!$user->isActivated()) {
            return $this->responseMessage(self::FAIL, __('auth.email_reset.email_has_not_been_activated'), 422);
        }

        $user->update([
            'activation_code' => $this->generateActivationCode(Config::get('auth.code_generate.length', 0)),
        ]);

        $this->sendEmail($user);

        return $this->responseMessage(self::SUCCESS, __('auth.email_reset.send_reset_password_verification_email_success'), 200);
    }

    /**
     * Resend reset password verification to user email.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendVerificationEmail(EmailIsExists $request)
    {
        $user = User::whereEmail($request->email)->first();

        if (!$user->activation_code) {
            return $this->responseMessage(self::FAIL, __('auth.email_reset.activation_code_null'), 422);
        };

        $this->sendEmail($user);

        return $this->responseMessage(self::SUCCESS, __('auth.email_reset.send_reset_password_verification_email_success'), 200);
    }

    /**
     * Validate activation code.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateResetCode(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'activation_code' => 'required',
        ]);

        $user = User::whereEmail(base64url_decode($request->email))->first();

        if (!$user) {
            return $this->responseMessage(self::FAIL, __('auth.email_reset.unauthenticated_email'), 422);
        }

        if ($user->activation_code != $request->activation_code) {
            return $this->responseMessage(self::FAIL, __('auth.email_reset.activation_code_wrong'), 422);
        }

        return $this->responseMessage(self::SUCCESS, __('auth.email_reset.activation_code_valid'), 200);
    }

    /**
     * Reset user password.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'activation_code' => 'required',
            'password' => 'confirmed|required|string|min:6',
        ]);

        $user = User::whereEmail(base64url_decode($request->email))->first();

        if (!$user) {
            return $this->responseMessage(self::FAIL, __('auth.email_reset.unauthenticated_email'), 422);
        }

        if ($user->activation_code != $request->activation_code) {
            return $this->responseMessage(self::FAIL, __('auth.email_reset.activation_code_wrong'), 422);
        }

        $user->password = Hash::make($request->password);
        $user->activation_code = null;
        $user->save();

        return $this->responseMessage(self::SUCCESS, __('auth.email_reset.reset_password_success'), 200);
    }

    /** Send the email */
    public function sendEmail(User $user) :void
    {
        Mail::to($user)->send(new UserResetPassword($user));
    }

    protected function generateActivationCode(int $digit = 6): int
    {
        return rand((int)str_repeat('1', $digit), (int)str_repeat('9', $digit));
    }
}
