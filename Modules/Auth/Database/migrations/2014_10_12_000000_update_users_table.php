<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->string('username')->nullable();
            $table->string('avatar')->nullable();
            $table->timestamp('banned_at')->nullable();
            $table->string('activation_code')->nullable();
            $table->integer('level')->default(1);
            $table->timestamp('last_online')->nullable();
            $table->softDeletes();
            $table->json('meta')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('username')->nullable();
            $table->dropColumn('avatar');
            $table->dropColumn('banned_at');
            $table->dropColumn('activation_code');
            $table->dropColumn('level');
            $table->dropColumn('last_online');
            $table->dropColumn('deleted_at');
            $table->dropColumn('meta');
        });
    }
}
