<?php

namespace Modules\Auth\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class UserActivity extends Model
{
    // type
    const BIODATA = 1;
    const IDENTITY = 2;
    const ACTIVITY = 4;

    // activity code
    const LOGIN = 41;
    const LOGOUT = 42;
    const REGISTER = 43;

    // activity code
    const PROFILE_FULL_NAME_UPDATED = 11;
    const PROFILE_BIRTHDAY_UPDATED = 12;
    const PROFILE_GENDER_UPDATED = 13;
    const PROFILE_USERNAME_UPDATED = 14;
    const PROFILE_AVATAR_UPDATED = 15;

    /** @var string */
    protected $table = 'user_activities';

    /** @var array */
    protected $fillable = [
        'id',
        'user_id',
        'type',
        'activity_code',
        'details',
    ];

    /** @var array */
    protected $casts = [
        'details' => 'object',
    ];

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
