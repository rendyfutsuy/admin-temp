<?php

namespace Modules\Auth\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserResetPassword extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /** @var \Modules\Auth\Models\User */
    protected $user;

    public $subject = "Ubah kata sandi Akun HolaHalo";

    /**
     * Create a new message instance.
     *
     * @param  \Modules\Auth\Models\User  $user
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(Config::get('mail.default_email'))
            ->view('auth.emails.reset-password-with-activation-code')
            ->with([
                'userName' => $this->user->name,
                'activationCode' => $this->user->getActivationCode(),
                'encodedEmail' => base64url_encode($this->user->email),
            ]);
    }
}
